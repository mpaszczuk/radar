#include "GSM.h"

/*GSM packet*/
static volatile uint8_t com_gsm[4][128] = {
    "AT+CMGF=1\r",
    "AT+CMGS=\"+48698984888\"\r",
    "test1",
    {26, 0}}; /**< packet of gsm data, including config, phone number, message,
                 eof**/
static volatile com_gsm_ptr =
    1; /**< variable to go through a packet of gsm data**/

/**
 * @brief This function sends a text message
 * @retval None
 */
void sendMessageGSM(void) {
  /*This volatile global variable- com_gsm_ptr is used to send the whole packet
   of data through GSM. com_gsm[4][128]*/
  com_gsm_ptr = 1;
  HAL_UART_Transmit_IT(&huart3, com_gsm[0], strlen((char *)com_gsm[0]));
}

void sendMessageGSMCallback(data_cooked *ptr) {
  /*Sending a whole GSM packet. sendMessageGSM() sends first message
  (com_gsm[0]) and it generates interrupt with callback after completed
  transmission. Clbk transmits thorugh another message thorugh interrupt,
  dependind on value of com_gsm_ptr. For example, after fist message com_gsm_ptr
  is set to 1 so clbk transmits message from com_gsm[1]. After that the third
  and (after that) fourth message is sent, so eventually the whole packet is
  sent*/
  if (com_gsm_ptr % 4 != 0) {
    uint8_t *msg = com_gsm[com_gsm_ptr];
    volatile i = 0;
    while (i < 30000) {
      ++i;
    }
    sprintf(com_gsm[2], "fotodiode:%d ultrasonic:%d",
            (int)ptr->cooked_fotodiode_data, (int)ptr->cooked_ultrasonic_data);
    HAL_UART_Transmit_IT(&huart3, msg, strlen(msg));
    com_gsm_ptr++;
  } else {
    com_gsm_ptr = 1;
  }
}