/**
 * @file stepper_motor.h
 * @brief Functions to move stepper motor
 * @author Magdalena Paszczuk
 */
#ifndef STEPPER_MOTOR_H
#define STEPPER_MOTOR_H
#include "gpio.h"

/**
 * @brief This function drives a stepper motor, 180 degrees clockwise and another 180 counterclockwise
 * @retval None
 */
void motorOn(void);

#endif /*STEPPER_MOTOR_H*/