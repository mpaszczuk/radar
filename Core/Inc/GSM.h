/**
 * @file stepper_motor.h
 * @brief Functions to move stepper motor
 * @author Magdalena Paszczuk
 */
#ifndef GSM_H
#define GSM_H
#include "data_processing.h"
#include "stm32f3xx_hal.h"
#include "usart.h"

void sendMessageGSM(void);
void sendMessageGSMCallback(data_cooked *ptr);

#endif /*GSM*/
