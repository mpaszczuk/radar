/**
 * @file data_processing.h
 * @brief All the functions necessary to processs data correclty, so they can be send through GSM/Bluetooth
 * @author Magdalena Paszczuk
 */

#ifndef DATA_PROCESSING_H
#define DATA_PROCESSING_H
#include "stm32f3xx_hal.h"
#include <math.h>
#include <stdio.h>

/**
 * @brief This structure hold output voltage from adc data
 */
typedef struct
{	
	float raw_fotodiode_data; /**< voltage derived from fotodiode sensor, **/
	float raw_ultrasonic_data; /**< voltage derived from ultrasonic sensor**/
} data_raw;

/**
 * @brief This structure hold cooked data- distance in cm
 */
typedef struct
{	
	float cooked_fotodiode_data; /**< computed distance from fotodiode sensor**/
	float cooked_ultrasonic_data; /**< computed distance from ultrasonic sensor**/
} data_cooked;

/**
 * @brief This function caluclates output voltage from adc data
 * @param adc_val uint16_t value taken from adc
 * @retval float- calculated voltage
 */
float paramterizeOutputVoltage(uint16_t adc_val);

/**
 * @brief This function computes fotodiode distance based on datasheet info and values from sensors
 * @param ptr volatile data_raw pointer to a data structure that holds a place for raw data
 * @param p volatile data_cooked pointer to a data structure that holds a place for calculated data
 * @retval None
 */
void computeFotodiodeDistance(volatile data_raw* ptr, volatile data_cooked* p);

/**
 * @brief This function computes ultrasonic distance based on datasheet info and values from sensors
 * @param ptr volatile data_raw pointer to a data structure that holds a place for raw data
 * @param p volatile data_cooked pointer to a data structure that holds a place for calculated data
 * @retval None
 */
void computeUltrasonicDistance(volatile data_raw* ptr, volatile data_cooked* p);

/**
 * @brief This function checks if the detection was made
 * @param ptr volatile data_cooked pointer to a data structure that holds a place for calculated data
 * @retval None
 */
void checkForData(volatile data_cooked* ptr);

/**
 * @brief This function is for parsing data to send
 * @param ptr volatile data_cooked pointer to a data structure that holds a place for calculated data
 * @retval parsed message uint8_t
 */
uint8_t parseData(volatile data_cooked* ptr);

#endif /*DATA_PROCESSING_H*/
