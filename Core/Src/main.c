/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "data_processing.h"
#include <string.h>
#include "stepper_motor.h"
#include "GSM.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/*Ptointers to go through receive buffers in order to get whole message.
My project needn't to receive anything, it is more so to check whether
the communication between sensor and ucontroller is correct. */
volatile uint8_t ptr = 0; /**< variable to go thorugh buffer_rx **/
volatile uint8_t ptr_uart3 = 0; /**< variable to go thorugh buffer_rx_uart3 **/
volatile uint8_t buffer_rx[256]; /**< buffer for received data from uart2**/
volatile uint8_t buffer_rx_uart3[256]; /**< buffer for received data from uart3**/

/*Flag that controlls flow of data */
extern volatile uint8_t object_detected; /**< flag signalizing whether objects was detected or not**/

/*Instances for data */
static volatile data_raw data; /**< object for raw data from sensors after calcuating voltage**/
static volatile data_cooked val; /**< object for cooked data from sensors- distance in cm**/

/*Variables for values from ADC*/
volatile uint16_t adc_val1; /**< value from adc1 derived from fotodiode sensor**/
volatile uint16_t adc_val2; /**< value from adc2 derived from ultrasonic sensor**/

//volatile uint8_t hc05_mess[256]; /**< data buffer for parsed data to send through HC-05 Bluetooth**/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void getData();
//void motorOn(void);
void sendMessageGSM(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/**
 * @brief Callback after completed conversion from adc- gets val, computes output voltage and calculates it for distance.
 * @param hadc pointer adc handle
 * @return None
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {
  if (hadc == &hadc1) {
    adc_val1 = HAL_ADC_GetValue(&hadc1);
    data.raw_fotodiode_data = paramterizeOutputVoltage(adc_val1);
    computeFotodiodeDistance(&data, &val);

  } else if (hadc == &hadc2) {
    adc_val2 = HAL_ADC_GetValue(&hadc2);
    data.raw_ultrasonic_data = paramterizeOutputVoltage(adc_val2);
    computeUltrasonicDistance(&data, &val);
  }
}

/**
 * @brief Callback after completed transmission UART. The callback is used to send a whole GSM packet
 * @param huart pointer uart handle
 * @return None
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
  if (huart == &huart3) {
    sendMessageGSMCallback(&val);
  }
}

/**
 * @brief Callback after completed reception UART. The callback is to retrieve a whole message, byte after byte
 * @param huart pointer uart handle
 * @return None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  /*In both UART2 and UART3 the full message is received byte after byte*/
  if (huart == &huart2) {
    __NOP();
    ptr++;
    HAL_UART_Receive_IT(&huart2, buffer_rx + ptr, 1);
  } else if (huart == &huart3) {
    __NOP();
    ptr_uart3++;
    HAL_UART_Receive_IT(&huart3, buffer_rx_uart3 + ptr_uart3, 1);
  }
}

/**
 * @brief Callback after completed a certain time period. The callback is used to send data thorugh GSM, get data from sensors, and send them thorugh bluetooth
 * @param huart pointer uart handle
 * @return None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim == &htim3) {
    checkForData(&val);
    if (object_detected) {
      /*Data parsing*/
      volatile uint8_t *hc05_mess; /**< data buffer for parsed data to send through HC-05 Bluetooth**/
      hc05_mess=parseData(&val);
      //sprintf(hc05_mess, "fotodiode:%d ultrasonic:%d\n",(int)val.cooked_fotodiode_data, (int)val.cooked_ultrasonic_data);
      HAL_UART_Transmit_IT(&huart2, hc05_mess, strlen(hc05_mess));
    }
  } else if (htim == &htim2) {
    getData();
  } else if (htim == &htim4) {
    sendMessageGSM();
  } 
  if (htim == &htim8) {
    motorOn();
  }
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM3_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  MX_TIM8_Init();
  /* USER CODE BEGIN 2 */

  HAL_TIM_Base_Start_IT(&htim2);
  HAL_TIM_Base_Start_IT(&htim4);
  HAL_TIM_Base_Start_IT(&htim3);
  HAL_TIM_Base_Start_IT(&htim8);
  HAL_UART_Receive_IT(&huart3, buffer_rx_uart3 + ptr_uart3, 1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {
    // motorOn();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

    // sendMessageGSM();
    // HAL_Delay(23000);
    // asm("NOP");
    // HAL_Delay(1000);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3
                              |RCC_PERIPHCLK_TIM8|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  PeriphClkInit.Tim8ClockSelection = RCC_TIM8CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/**
 * @brief This function gets data from sensors thorugh DMA
 * @retval None
 */
void getData(void) {
  HAL_ADC_Start_IT(&hadc1);
  HAL_ADC_Start_IT(&hadc2);
}

/**
 * @brief This function drives a stepper motor, 180 degrees clockwise and another 180 counterclockwise
 * @retval None
 */
// void motorOn(void) {

//   /*Clockwise*/
//   for (int i = 0; i < 256; i++) {

//     HAL_Delay(2);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);

//     HAL_Delay(2);
//   }
//   /*Counterclockwise*/
//   for (int i = 0; i < 256; i++) {
//     HAL_Delay(2);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);

//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

//     HAL_Delay(2);

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
    
//     HAL_Delay(2);
//   }
// }


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line
     number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line)
   */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
