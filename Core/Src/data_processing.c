/**
 * @file data_processing.c
 * @brief All the implementations of functions necessary to processs data correclty, so they can be send through GSM/Bluetooth
 * @author Magdalena Paszczuk
 */
#include "data_processing.h"

volatile uint8_t object_detected=0; /**< flag signalizing whether objects was detected or not**/

float paramterizeOutputVoltage(uint16_t adc_val)
{
    float voltage;
    voltage=(float)(3*adc_val)/4095;
    return voltage;
}

void computeFotodiodeDistance(volatile data_raw* ptr, volatile data_cooked* p)
{
	  p->cooked_fotodiode_data= 27*(powf(ptr->raw_fotodiode_data, (-1.27))); 
}

void computeUltrasonicDistance(volatile data_raw* ptr, volatile data_cooked* p)
{
    p->cooked_ultrasonic_data = ((ptr->raw_ultrasonic_data)*520)/3; //3V
}

void checkForData(volatile data_cooked* ptr)
{
	if(ptr->cooked_ultrasonic_data>490 || ptr->cooked_fotodiode_data<10)
	{
		object_detected=0;
	}
  else
  {
    object_detected=1;
  }
}

uint8_t parseData(volatile data_cooked* ptr)
{
  static uint8_t mess[256];
	sprintf(mess, "fotodiode:%d ultrasonic:%d\n",(int)ptr->cooked_fotodiode_data, (int)ptr->cooked_ultrasonic_data);
  return mess;
}

